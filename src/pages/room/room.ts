import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RoomDetailPage } from '../room-detail/room-detail';

@IonicPage()
@Component({
  selector: 'page-room',
  templateUrl: 'room.html',
})
export class RoomPage {
  session: any;

  constructor(public navParams: NavParams, public navCtrl: NavController) {
    this.session = navParams.data.session;
  }

  roomSelected(room: any) {
    this.navCtrl.push(RoomDetailPage, {
      name: room.name,
      apresentacoes: room.apresentacoes
    });
  }
}
