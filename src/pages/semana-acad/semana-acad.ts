import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the SemanaAcadPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-semana-acad',
  templateUrl: 'semana-acad.html',
})
export class SemanaAcadPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public app: App) {
  }

}
