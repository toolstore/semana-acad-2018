import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SemanaAcadPage } from './semana-acad';

@NgModule({
  declarations: [
    SemanaAcadPage,
  ],
  imports: [
    IonicPageModule.forChild(SemanaAcadPage),
  ],
})
export class SemanaAcadPageModule {}
