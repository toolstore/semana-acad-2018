import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConferenceData } from '../../providers/conference-data';

@IonicPage()
@Component({
  selector: 'page-list-minicursos',
  templateUrl: 'list-minicursos.html',
})
export class ListMinicursosPage {
  sessions  : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public confData: ConferenceData,) {

    this.confData.getMiniCursos().subscribe((data: any) => {
      console.log(data);
      this.sessions = data;
    });
  }

  apSelected(session: any) {
    this.navCtrl.push(ListMinicursosPage, session);
  }
}
