import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListMinicursosPage } from './list-minicursos';

@NgModule({
  declarations: [
    ListMinicursosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListMinicursosPage),
  ],
})
export class ListMinicursosPageModule {}
