import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CredenciamentoPage } from './credenciamento';

@NgModule({
  declarations: [
    CredenciamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(CredenciamentoPage),
  ],
  exports: [
    CredenciamentoPage
  ]
})
export class CredenciamentoPageModule {}
