import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ApOraisPage } from './ap-orais';

@NgModule({
  declarations: [
    ApOraisPage,
  ],
  imports: [
    IonicPageModule.forChild(ApOraisPage),
  ],
  exports: [
    ApOraisPage
  ]
})
export class ApOraisPageModule {}
