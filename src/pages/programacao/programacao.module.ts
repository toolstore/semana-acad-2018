import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProgramacaoPage } from './programacao';

@NgModule({
  declarations: [
    ProgramacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(ProgramacaoPage),
  ],
  exports: [
    ProgramacaoPage
  ]
})
export class ProgramacaoPageModule {}
