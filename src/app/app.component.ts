import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, Nav, Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { MapPage } from '../pages/map/map';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { ProgramacaoPage } from '../pages/programacao/programacao';
import { SemanaAcadPage } from "../pages/semana-acad/semana-acad";
import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { CredenciamentoPage } from '../pages/credenciamento/credenciamento';
import { ListMinicursosPage } from '../pages/list-minicursos/list-minicursos';


export interface PageInterface {
  title: string;
  component: any;
  icon: string;
  index?: number;
}

@Component({
  templateUrl: 'app.template.html'
})
export class ConferenceApp {
  // the root nav is a child of the root app component
  // @ViewChild(Nav) gets a reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  // List of pages that can be navigated to from the left menu
  appPages: PageInterface[] = [

    { title: 'Conheça o aplicativo', component: TutorialPage, index: 2, icon: 'md-phone-portrait' },
    { title: 'Semana acadêmica', component: SemanaAcadPage, index: 3, icon: 'book' },
    { title: 'Credenciamento', component: CredenciamentoPage, index: 4, icon: 'md-list-box' },
    { title: 'Programação', component: ProgramacaoPage, index: 0, icon: 'calendar' },
    { title: 'Minicursos', component: ListMinicursosPage, index: 1, icon: 'book' },
    { title: 'Mapa', component: MapPage, icon: 'map' },
  ];
  rootPage: any;

  constructor(
    public events: Events,
    public userData: UserData,
    public menu: MenuController,
    public platform: Platform,
    public confData: ConferenceData,
    public storage: Storage,
    public splashScreen: SplashScreen
  ) {

    // Check if the user has already seen the tutorial
    this.storage.get('hasSeenTutorial')
      .then((hasSeenTutorial) => {
        if (hasSeenTutorial) {
          this.rootPage = ProgramacaoPage;
        } else {
          this.rootPage = TutorialPage;
        }
        this.platformReady()
      });

    // load the conference data
    confData.load();
  }

  //
  openPage(page: PageInterface) {
    let params = {};
    if (this.nav.getActiveChildNav() && page.index != undefined) {
      this.nav.getActiveChildNav().select(page.index);
      // Set the root of the nav with params if it's a index
    } else {
      this.nav.setRoot(page.component, params).catch((err: any) => {
        console.log(`Didn't set nav root: ${err}`);
      });
    }

  }

  openTutorial() {
    this.nav.setRoot(TutorialPage);
  }

  platformReady() {
    // Call any initial plugins when ready
    this.platform.ready().then(() => {
      this.splashScreen.hide();
    });
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNav();

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.component) {
        return 'primary';
      }
      return;
    }

    if (this.nav.getActive() && this.nav.getActive().name === page.component) {
      return 'primary';
    }
    return;
  }
}
  